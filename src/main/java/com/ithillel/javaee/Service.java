package com.ithillel.javaee;

import java.util.Scanner;

public class Service {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Formater formater = new Formater();

		System.out.println("Human readable duration format");
		System.out.println("Please, enter seconds : ");
		System.out.println(formater.format(scanner.nextInt()));
	}
}