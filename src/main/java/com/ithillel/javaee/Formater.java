package com.ithillel.javaee;

import lombok.Data;

@Data
public class Formater {
	private final int SECONDS_IN_MINUTE = 60;
	private final int SECONDS_IN_HOUR = 3600;
	private final int SECONDS_IN_DAY = 86400;
	private final int SECONDS_IN_YEAR = 31_536_000;

	private enum KindOfDuration {
		YEAR, DAY, HOUR, MINUTE, SECOND
	}

	public String format(int seconds) {
		if (seconds < 0) {
			return "Only positive arguments";
		}

		if (seconds == 0) {
			return "now";
		}

		int[] durations = new int[5];
		durations[0] = seconds / SECONDS_IN_YEAR;
		seconds = seconds % SECONDS_IN_YEAR;

		durations[1] = seconds / SECONDS_IN_DAY;
		seconds = seconds % SECONDS_IN_DAY;

		durations[2] = seconds / SECONDS_IN_HOUR;
		seconds = seconds % SECONDS_IN_HOUR;

		durations[3] = seconds / SECONDS_IN_MINUTE;
		seconds = seconds % SECONDS_IN_MINUTE;

		durations[4] = seconds;

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < durations.length; i++) {
			append(builder, durations[i], KindOfDuration.values()[i]);
		}

		builder.delete(builder.length() - 2, builder.length());
		String result = builder.toString();

		if (result.lastIndexOf(",") == -1) {
			return result;
		}

		return result.substring(0, builder.lastIndexOf(",")) + " and"
				+ result.substring(builder.lastIndexOf(",") + 1);
	}

	private void append(StringBuilder stringBuilder, int duration, KindOfDuration kindOfDuration) {
		if (duration != 0) {
			stringBuilder.append(duration);
			stringBuilder.append(duration == 1 ? " " + kindOfDuration.toString().toLowerCase() + ", "
					: " " + kindOfDuration.toString().toLowerCase() + "s, ");
		}
	}
}